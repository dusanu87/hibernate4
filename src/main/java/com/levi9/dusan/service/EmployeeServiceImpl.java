package com.levi9.dusan.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.levi9.dusan.model.Employee;
import com.levi9.dusan.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired 
	public EmployeeRepository employeeRepository;

	@Transactional
	public List<Employee> findAllEmployee() {
		// TODO Auto-generated method stub
		return employeeRepository.findAll();
	}

	
	public Employee findById(Long id) {
		// TODO Auto-generated method stub
		return employeeRepository.findById(id);
	}
	
	@Transactional
	public void addEmployee(Employee employee) {
		// TODO Auto-generated method stub
		employeeRepository.save(employee);
	}

	@Transactional
	public void updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		employeeRepository.update(employee);
	}

	@Transactional
	public void removeEmployee(Long id) {
		// TODO Auto-generated method stub
		employeeRepository.remove(id);
	}

	
}
