package com.levi9.dusan.service;

import java.util.List;

import com.levi9.dusan.model.Address;

public interface AddressService {

	public List<Address> findAllAddress();
	public Address findById(Long id);
	public void addAddress(Address address);
	public void updateAddress(Address address);
	public void removeAddress(Long id);
	 
}
