package com.levi9.dusan.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.levi9.dusan.model.Address;
import com.levi9.dusan.repository.AddressRepository;

@Service
public class AddressServiceImpl implements AddressService {

	@Autowired
	AddressRepository addressRepository;
	
	@Transactional
	public List<Address> findAllAddress() {
		// TODO Auto-generated method stub
		return addressRepository.findAll();
	}

	public Address findById(Long id) {
		// TODO Auto-generated method stub
		return addressRepository.findById(id);
	}

	@Transactional
	public void addAddress(Address address) {
		// TODO Auto-generated method stub
		addressRepository.save(address);
	}

	@Transactional
	public void updateAddress(Address address) {
		// TODO Auto-generated method stub
		addressRepository.update(address);
	}

	@Transactional
	public void removeAddress(Long id) {
		// TODO Auto-generated method stub
		addressRepository.remove(id);
	}

}
