package com.levi9.dusan.service;

import java.util.List;

import com.levi9.dusan.model.Employee;


public interface EmployeeService {

	 public List<Employee> findAllEmployee();
	 public Employee findById(Long id);
	 public void addEmployee(Employee employee);
	 public void updateEmployee(Employee employee);
	 public void removeEmployee(Long id);
}
