package com.levi9.dusan.repository;

import com.levi9.dusan.genericrepository.GenericRepository;
import com.levi9.dusan.model.Address;

public interface AddressRepository extends GenericRepository<Address> {

}
