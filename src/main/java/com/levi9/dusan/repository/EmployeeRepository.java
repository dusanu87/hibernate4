package com.levi9.dusan.repository;

import com.levi9.dusan.genericrepository.GenericRepository;
import com.levi9.dusan.model.Employee;

public interface EmployeeRepository extends GenericRepository<Employee> {

}
