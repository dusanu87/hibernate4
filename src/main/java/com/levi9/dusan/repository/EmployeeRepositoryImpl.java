package com.levi9.dusan.repository;

import org.springframework.stereotype.Repository;

import com.levi9.dusan.genericrepository.GenericRepository;
import com.levi9.dusan.genericrepository.GenericRepositoryImpl;
import com.levi9.dusan.model.Employee;

@Repository
public class EmployeeRepositoryImpl extends GenericRepositoryImpl<Employee> implements EmployeeRepository{

	protected EmployeeRepositoryImpl() {
		super(Employee.class);
		// TODO Auto-generated constructor stub
	}
	
}
