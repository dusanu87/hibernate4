package com.levi9.dusan.repository;

import org.springframework.stereotype.Repository;

import com.levi9.dusan.genericrepository.GenericRepositoryImpl;
import com.levi9.dusan.model.Address;

@Repository
public class AddressRepositoryImpl extends GenericRepositoryImpl<Address> implements AddressRepository {

	protected AddressRepositoryImpl() {
		super(Address.class);
		// TODO Auto-generated constructor stub
	}

}
