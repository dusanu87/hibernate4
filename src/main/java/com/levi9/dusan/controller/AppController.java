package com.levi9.dusan.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.levi9.dusan.model.Address;
import com.levi9.dusan.model.Employee;
import com.levi9.dusan.service.AddressService;
import com.levi9.dusan.service.EmployeeService;

@Controller
public class AppController {
	
	
	EmployeeService employeeService;
	AddressService addressService;
	
	@Autowired
	public AppController(EmployeeService employeeService, AddressService addressService){
		this.employeeService = employeeService;
		this.addressService = addressService;
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)  
	public ModelAndView welcome() {  
	  return new ModelAndView("proba");  
	 }

	@RequestMapping(value ="/employees", method = RequestMethod.GET)
	public String getAll(Map<String, Object> map){
		map.put("employee", new Employee());
		map.put("employeeList", employeeService.findAllEmployee());
		
		return "employee";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView saveEmployee(@ModelAttribute("employee")
	Employee employee, BindingResult result) {
				
		Employee emp = employee;
		
		System.out.println("ADD: " + emp.getFirstName());
		System.out.println("ADD: " + emp.getLastName());
		System.out.println("ADD: " + emp.getUsername());
		System.out.println("ADD: " + emp.getPassword());
		
		employeeService.addEmployee(emp);
		
		return new ModelAndView("redirect:/proba"); 
		 
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public ModelAndView deleteEmployee(@ModelAttribute("employee")
	Employee employee, BindingResult result) {
		
		Employee emp = employee;
		
		employeeService.addEmployee(emp);
		
		return new ModelAndView("redirect:/login"); 
		 
	}
	
	@RequestMapping(value ="/address", method = RequestMethod.GET)
	public String getAllAddress(Map<String, Object> map){
		map.put("address", new Address());
		map.put("addressList", addressService.findAllAddress());
		
		return "address";
	}
	
	@RequestMapping(value = "/saveAddress", method = RequestMethod.POST)
	public ModelAndView saveAddress(@ModelAttribute("address")
	Address address, BindingResult result) {
				
		Address addr = address;
		Employee emp = employeeService.findById(addr.getEmployeeId());
		emp.setAddress(addr);
		addr.setEmployee(emp);
		
		addressService.addAddress(addr);
		
		return new ModelAndView("redirect:/login"); 
		 
	}
	
}
