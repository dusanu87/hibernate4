package com.levi9.dusan.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.levi9.dusan.model.Employee;
import com.levi9.dusan.service.EmployeeService;

@Controller
@RequestMapping(value = "/emp")
public class EmpController {

	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value =  "/json/get/emp", method = RequestMethod.GET)
	public @ResponseBody List<Employee> getEmployees() {
		
		List<Employee> names = new ArrayList<Employee>();
		names  = employeeService.findAllEmployee();
		
		return names;
	}
	
	@RequestMapping(value = "/json/get/{id}", method = RequestMethod.GET)
    public @ResponseBody Employee getEmployee(@PathVariable("id") Long empId) {
        
        employeeService.findAllEmployee();
        
        return employeeService.findById(empId);
    }
	
	@RequestMapping(value =  "/json/add",method = RequestMethod.POST)
	public @ResponseBody Employee addEmployee(@RequestBody Employee emp) {
		
		employeeService.addEmployee(emp);
		
		return emp;
	}
	
	@RequestMapping(value = "/json/edit/emp", method = RequestMethod.PUT)
    public @ResponseBody String editEmployee(@RequestBody Employee emp) {
        
        employeeService.updateEmployee(emp);
        
        return "Successfully edited!";
    }
	
	@RequestMapping(value = "json/delete/{id}", method = RequestMethod.DELETE)
    public @ResponseBody String  deleteEmployee(@PathVariable("id") Long empId) {
        
        employeeService.removeEmployee(empId);
        
        return "Successfully deleted!";
    }

	
}
