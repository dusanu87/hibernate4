package com.levi9.dusan.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import com.levi9.dusan.model.Address;
import com.levi9.dusan.service.AddressService;

@Controller
@RequestMapping(value = "/address")
public class AddressController {

	@Autowired
	private AddressService addressService;
	
	@RequestMapping(value =  "/json/get/address", method = RequestMethod.GET)
	public @ResponseBody List<Address> getAddresss() {
		
		List<Address> names = new ArrayList<Address>();
		names  = addressService.findAllAddress();
		
		return names;
	}
	
	@RequestMapping(value = "/json/get/{id}", method = RequestMethod.GET)
    public @ResponseBody Address getAddress(@PathVariable("id") Long addressId) {
        
        addressService.findAllAddress();
        
        return addressService.findById(addressId);
    }
	
	@RequestMapping(value =  "/json/add",method = RequestMethod.POST)
	public @ResponseBody Address addAddress(@RequestBody Address address) {
		
		addressService.addAddress(address);
		
		return address;
	}
	
	@RequestMapping(value = "/json/edit/address", method = RequestMethod.PUT)
    public @ResponseBody String editAddress(@RequestBody Address emp) {
        
        addressService.updateAddress(emp);
        
        return "Successfully edited!";
    }
	
	@RequestMapping(value = "json/delete/{id}", method = RequestMethod.DELETE)
    public @ResponseBody String  deleteAddress(@PathVariable("id") Long addressId) {
        
        addressService.removeAddress(addressId);
        
        return "Successfully deleted!";
    }

	
}
