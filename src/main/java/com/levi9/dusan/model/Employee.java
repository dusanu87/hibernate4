package com.levi9.dusan.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="EMPLOYEE")
public class Employee implements Serializable {

	 	
		private static final long serialVersionUID = 7884957739792951898L;

		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Column(name = "EMPLOYEE_ID", unique = true, nullable = false)
	 	private Long id;
	 	
	 	@Column(name = "FIRST_NAME", nullable = false)
	 	private String firstName;
	 	
	 	@Column(name = "LAST_NAME", nullable = false)
	 	private String lastName;
	 	
	 	@Column(name = "USERNAME", nullable = false )
	 	private String username;
	 	
	 	@Column(name = "PASSWORD", nullable = false )
	 	private String password;
	 	
	 	@Temporal(TemporalType.DATE)
	 	@Column(name = "BIRTH_DATE", nullable = false)
		private Date birthDate;
	 	
	 	@OneToOne(mappedBy="employee", fetch = FetchType.LAZY ,cascade=CascadeType.ALL)
	 	private Address address;
	 	
	    public Employee() {
	    	
	    }
	    
	    public Employee(String firstName, String lastName, String username, String password, Date birthDate) {
	    	this.firstName = firstName;
	    	this.lastName = lastName;
	    	this.username = username;
	    	this.password = password;
	    	this.birthDate = birthDate;
	    }

	    public Long getId() {
			return id;
		}
	    
		public void setId(Long id) {
			this.id = id;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public Date getBirthDate() {
			return birthDate;
		}

		public void setBirthDate(Date birthDate) {
			this.birthDate = birthDate;
		}

		public Address getAddress() {
			return address;
		}

		public void setAddress(Address address) {
			this.address = address;
		}

}
