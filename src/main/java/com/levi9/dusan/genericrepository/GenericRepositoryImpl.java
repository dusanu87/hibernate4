package com.levi9.dusan.genericrepository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


public class GenericRepositoryImpl<Entity> implements GenericRepository<Entity> {

	private Class<Entity> entityClass;
	 
    protected GenericRepositoryImpl(Class<Entity> entityClass) {
        this.entityClass = entityClass;
    }
    
    @PersistenceContext
   	private EntityManager entityManager;
	
	public List<Entity> findAll() {
		// TODO Auto-generated method stub
		return entityManager.createQuery("from " + entityClass.getSimpleName()).getResultList();
	}

	public Entity findById(Long id) {
		// TODO Auto-generated method stub
		return entityManager.find(entityClass, id);
	}
	
	public void save(Entity e) {
		// TODO Auto-generated method stub
		entityManager.persist(e);
	}

	public void update(Entity e) {
		// TODO Auto-generated method stub
		entityManager.merge(e);
	}

	public void remove(Long id) {
		// TODO Auto-generated method stub
		Entity ent = findById(id);
		entityManager.remove(ent);
	}
	
}
