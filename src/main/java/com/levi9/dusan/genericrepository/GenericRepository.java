package com.levi9.dusan.genericrepository;

import java.util.List;

public interface GenericRepository<Entity> {

	List<Entity> findAll();
	Entity findById(Long id);
	void save(Entity e);
	void update(Entity e);
	void remove(Long id);
	
}
