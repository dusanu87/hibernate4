<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<title>Spring 4 MVC JPA - Employee</title>
	
	<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
  	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
  	<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
  	<script>
  		$(function() {
  			$("#startdate").datepicker({
  	  		   onSelect: function(dateText, inst) { 
  	  		      var dateAsString = dateText; 
  	  		      var dateAsObject = $(this).datepicker( 'getDate' ); 
  	  		   }
  	  		});
    	$("#requestdate").datepicker({
	  		   onSelect: function(dateText, inst) { 
	  	  		      var dateAsString = dateText; 
	  	  		      var dateAsObject = $(this).datepicker( 'getDate' ); 
	  	  		   }
	  	  		});
    	$("#endingdate").datepicker({
	  		   onSelect: function(dateText, inst) { 
	  	  		      var dateAsString = dateText; 
	  	  		      var dateAsObject = $(this).datepicker( 'getDate' ); 
	  	  		   }
	  	  		});
  		});
  		
  	</script>
</head>
<body>

<h3>Employee</h3>
 
<form:form method="post" action="save.html" commandName="employee">

	<table>
	<tr>
		<td><form:label path="id">Id</form:label></td>
		<td><form:input path="id" /></td> 
	</tr>
	<tr>
		<td><form:label path="firstName">First name</form:label></td>
		<td><form:input path="firstName" /></td> 
	</tr>
	<tr>
		<td><form:label path="lastName">Last name</form:label></td>
		<td><form:input path="lastName" /></td> 
	</tr>
	<tr>
		<td><form:label path="username">Username</form:label></td>
		<td><form:input path="username" /></td> 
	</tr>
	<tr>
		<td><form:label path="password">Password</form:label></td>
		<td><form:input path="password" /></td>
	</tr>
	<tr>
		<td><form:label path="birthDate">Birth date</form:label></td>
		<td><form:input path="birthDate" /></td> 
	</tr>	
	<tr>
		<td colspan="2">
			<input type="submit" value="Submit"/>
		</td>
	</tr>
</table>	
</form:form>

	
<h3>Employee list</h3>
<c:if  test="${!empty employeeList}">
<table class="data">
<tr>
	<th>Id</th>
	<th>first name</th>
	<th>last name</th>
	<th>username</th>
	<th>password</th>
	<th>birth date</th>
</tr>
<c:forEach items="${employeeList}" var="employee">
	<tr>
		<td>${employee.id}</td>
		<td>${employee.firstName}</td>
		<td>${employee.lastName}</td>
		<td>${employee.username}</td>
		<td>${employee.password}</td>
		<td>${employee.birthDate}</td>
	</tr>
</c:forEach>
</table>
</c:if>


</body>
</html>
