<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<title>Spring 4 MVC JPA - Address</title>
	
	<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
  	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
  	<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
  	
</head>
<body>

<h3>Address</h3>
 
<form:form method="post" action="saveAddress.html" commandName="address">

	<table>
	<tr>
		<td><form:label path="employeeId">Id</form:label></td>
		<td><form:input path="employeeId" /></td> 
	</tr>
	<tr>
		<td><form:label path="street">Street</form:label></td>
		<td><form:input path="street" /></td> 
	</tr>
	<tr>
		<td><form:label path="city">City</form:label></td>
		<td><form:input path="city" /></td> 
	</tr>
	<tr>
		<td><form:label path="country">Country</form:label></td>
		<td><form:input path="country" /></td> 
	</tr>
		
	<tr>
		<td colspan="2">
			<input type="submit" value="Submit"/>
		</td>
	</tr>
</table>	
</form:form>

	
<h3>Address list</h3>
<c:if  test="${!empty addressList}">
<table class="data">
<tr>
	<th>Id</th>
	<th>street</th>
	<th>city</th>
	<th>country</th>
</tr>
<c:forEach items="${addressList}" var="address">
	<tr>
		<td>${address.employeeId}</td>
		<td>${address.street}</td>
		<td>${address.city}</td>
		<td>${address.country}</td>
	</tr>
</c:forEach>
</table>
</c:if>


</body>
</html>
